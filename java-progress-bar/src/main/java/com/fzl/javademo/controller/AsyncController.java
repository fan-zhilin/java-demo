package com.fzl.javademo.controller;

import com.fzl.javademo.aop.AsyncMsg;
import com.fzl.javademo.aop.EnableAsync;
import com.fzl.javademo.pojo.AsyncHolder;
import com.fzl.javademo.pojo.Result;
import com.fzl.javademo.service.AsyncService;
import com.fzl.javademo.service.imp.SSEServer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author fanzhilin
 */
@RestController
@RequestMapping(value = "/async")
@Api(tags = "接口异步示例")
@Slf4j
public class AsyncController {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private AsyncService asyncService;

    @EnableAsync
    @RequestMapping(value = "test", method = RequestMethod.POST)
    @ApiOperation(value = "接口测试")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "num", value = "数字", required = true, dataType = "int", paramType = "query", defaultValue = "1")
    })
    public Object demo(Integer num) throws InterruptedException {
        for (int i = 0; i < 15; i++) {
            AsyncMsg msg = AsyncHolder.get();
            String id = msg.getId();
            String s = redisTemplate.opsForValue().get("judge" + id).toString();
            if (s.equals("true")) {
                Thread.sleep(3000);
                //计算百分比
                String per = BigDecimal.valueOf(i).divide(BigDecimal.valueOf(15), 2, RoundingMode.HALF_DOWN).toString();
                //更新redis缓存任务进度
                asyncService.updateProgress(per);
            } else {
                return Result.success("结束啦");
            }
        }
        Integer b = 100;
        return Result.success(String.format("100除以%s的结果是%s", num, b / num));
    }

    @RequestMapping(value = "findAsyncMsg", method = RequestMethod.POST)
    @ApiOperation(value = "查询异步接口消息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "string", paramType = "query", defaultValue = "")
    })
    public Result<AsyncMsg> findAsyncMsg(String id) throws InterruptedException {
        AsyncMsg asyncMsg = asyncService.findAsyncMsg(id);
        SSEServer.batchSendMessage(String.valueOf(asyncMsg));
        return Result.success(asyncMsg);
    }

    @RequestMapping(value = "/close", method = RequestMethod.POST)
    @ApiOperation(value = "关闭任务接口")
    public Result<AsyncMsg> close(String id) {
        asyncService.close(id);
        return Result.success("关闭成功");
    }

    @GetMapping("/connect/{userId}")
    @ApiOperation("返回一个SseEmitter实例")
    public SseEmitter connect(@PathVariable String userId){
        return SSEServer.connect(userId);
    }

}
