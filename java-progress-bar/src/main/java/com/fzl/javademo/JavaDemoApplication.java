package com.fzl.javademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableAsync
@EnableSwagger2
public class JavaDemoApplication {
    public static void main(String[] args) {
        try {
            SpringApplication.run(JavaDemoApplication.class, args);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Bean
    public Docket docketA() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("java-progress-bar");
    }

}
