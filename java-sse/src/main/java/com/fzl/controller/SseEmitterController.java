package com.fzl.controller;

import com.fzl.javademo.pojo.Result;
import com.fzl.service.SseEmitterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.annotation.Resource;

/**
 * SSE长链接
 *
 * @author re
 * @date 2021/12/13
 */
@RestController
@RequestMapping("/sse")
@Slf4j
@CrossOrigin
public class SseEmitterController {
    @Resource
    private SseEmitterService sseEmitterService;


    @GetMapping("/createConnect")
    public SseEmitter createConnect(String clientId) {
        return sseEmitterService.createConnect(clientId);
    }


    @PostMapping("/broadcast")
    public void sendMessageToAllClient(@RequestBody(required = false) String msg) {
        sseEmitterService.sendMessageToAllClient(msg);
    }


    @PostMapping("/sendMessage")
    public void sendMessageToOneClient(@RequestBody(required = false) Result messageVo) {
        if (messageVo.getCode()==null) {
            return;
        }
        sseEmitterService.sendMessageToOneClient(messageVo.getId(), messageVo.getMsg());
    }


    @GetMapping("/closeConnect")
    public void closeConnect(@RequestParam(required = true) String clientId) {
        sseEmitterService.closeConnect(clientId);
    }

}
