package com.fzl.utlis;

import cn.hutool.core.map.MapUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

/**
 * @ClassName： SseEmitterUtils.java
 * @ClassPath： com.demo.utils.SseEmitterUtils.java
 * @Description： SSE 服务器发送事件
 * @Author： tanyp
 * @Date： 2022/9/13 11:03
 **/
@Slf4j
@Component
public class SseEmitterUtils {
    // 当前连接数
    private static AtomicInteger count = new AtomicInteger(0);
    // 存储 SseEmitter 信息
    private static Map<String, SseEmitter> sseEmitterMap = new ConcurrentHashMap<>();

    /**
     * @MonthName： connect
     * @Description： 创建用户连接并返回 SseEmitter
     * @Author： tanyp
     * @Date： 2022/9/13 11:09
     * @Param： [userId]
     * @return： org.springframework.web.servlet.mvc.method.annotation.SseEmitter
     **/
    public static SseEmitter connect(String key) {
        if (sseEmitterMap.containsKey(key)) {
            return sseEmitterMap.get(key);
        }
        try {
            // 设置超时时间，0表示不过期。默认30秒
            SseEmitter sseEmitter = new SseEmitter(0L);
            // 注册回调
            sseEmitter.onCompletion(completionCallBack(key));
            sseEmitter.onError(errorCallBack(key));
            sseEmitter.onTimeout(timeoutCallBack(key));
            sseEmitterMap.put(key, sseEmitter);
            // 数量+1
            count.getAndIncrement();
            return sseEmitter;
        } catch (Exception e) {
            log.info("创建新的SSE连接异常，当前连接Key为：{}", key);
        }
        return null;
    }

    /**
     * @MonthName： sendMessage
     * @Description： 给指定用户发送消息
     * @Author： tanyp
     * @Date： 2022/9/13 11:10
     * @Param： [userId, message]
     * @return： void
     **/
    public static void sendMessage(String key, String message) {
        if (sseEmitterMap.containsKey(key)) {
            try {
                SseEmitter sseEmitter = sseEmitterMap.get(key);
                sseEmitter.send(SseEmitter.event().data(message)
                        .id(key)
                        .reconnectTime(3000));
            } catch (IOException e) {
                log.error("用户[{}]推送异常:{}", key, e.getMessage());
                remove(key);
            }
        }
    }

    /**
     * @MonthName： groupSendMessage
     * @Description： 向同组人发布消息，要求：key + groupId
     * @Author： tanyp
     * @Date： 2022/9/13 11:15
     * @Param： [groupId, message]
     * @return： void
     **/
    public static void groupSendMessage(String groupId, String message) {
        if (MapUtil.isNotEmpty(sseEmitterMap)) {
            sseEmitterMap.forEach((k, v) -> {
                try {
                    if (k.startsWith(groupId)) {
                        v.send(message, MediaType.APPLICATION_JSON);
                    }
                } catch (IOException e) {
                    log.error("用户[{}]推送异常:{}", k, e.getMessage());
                    remove(k);
                }
            });
        }
    }

    /**
     * @MonthName： batchSendMessage
     * @Description： 广播群发消息
     * @Author： tanyp
     * @Date： 2022/9/13 11:15
     * @Param： [message]
     * @return： void
     **/
    public static void batchSendMessage(String message) {
        sseEmitterMap.forEach((k, v) -> {
            try {
                v.send(message, MediaType.APPLICATION_JSON);
            } catch (IOException e) {
                log.error("用户[{}]推送异常:{}", k, e.getMessage());
                remove(k);
            }
        });
    }

    /**
     * @MonthName： batchSendMessage
     * @Description： 群发消息
     * @Author： tanyp
     * @Date： 2022/9/13 11:16
     * @Param： [message, ids]
     * @return： void
     **/
    public static void batchSendMessage(String message, Set<String> ids) {
        ids.forEach(userId -> sendMessage(userId, message));
    }

    /**
     * @MonthName： remove
     * @Description： 移除连接
     * @Author： tanyp
     * @Date： 2022/9/13 11:17
     * @Param： [userId]
     * @return： void
     **/
    public static void remove(String key) {
        sseEmitterMap.remove(key);
        // 数量-1
        count.getAndDecrement();
        log.info("移除连接：{}", key);
    }

    /**
     * @MonthName： getIds
     * @Description： 获取当前连接信息
     * @Author： tanyp
     * @Date： 2022/9/13 11:17
     * @Param： []
     * @return： java.util.List<java.lang.String>
     **/
    public static List<String> getIds() {
        return new ArrayList<>(sseEmitterMap.keySet());
    }

    /**
     * @MonthName： getUserCount
     * @Description： 获取当前连接数量
     * @Author： tanyp
     * @Date： 2022/9/13 11:18
     * @Param： []
     * @return： int
     **/
    public static int getCount() {
        return count.intValue();
    }

    private static Runnable completionCallBack(String key) {
        return () -> {
            log.info("结束连接：{}", key);
            remove(key);
        };
    }

    private static Runnable timeoutCallBack(String key) {
        return () -> {
            log.info("连接超时：{}", key);
            remove(key);
        };
    }

    private static Consumer<Throwable> errorCallBack(String key) {
        return throwable -> {
            log.info("连接异常：{}", key);
            remove(key);
        };
    }

}

