package com.fzl.service;

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

public interface SseEmitterService {

    SseEmitter createConnect(String clientId);

    void sendMessageToAllClient(String msg);
    void sendMessageToOneClient(String code, String data);

    void closeConnect(String clientId);
}
