package com.fzl.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/threa")
@RestController
@Slf4j
@Api(tags = "测试两个线程获取变量")
public class TestStait {
    private  String name=new String();
    @RequestMapping(value = "/test1",method = RequestMethod.POST)
    @ApiOperation("线程1")
    public void thread1(){
        name="你好啊";
        int i = name.hashCode();
        System.out.println(i);
        System.out.println(name);
    }
    @RequestMapping(value = "/test2",method = RequestMethod.POST)
    @ApiOperation("线程2")
    public void thread2(){
        int i = name.hashCode();
        System.out.println(i);
        System.out.println(name);
    }
}
