package com.fzl.controller;

import com.fzl.javademo.pojo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/threa")
@RestController
@Slf4j
@Api(tags = "线程测试")
public class ThreadController {
    //只有当前线程才可以获取里面的值别的线程无法获取到里面的值
    private static ThreadLocal<String> threadLocal = new ThreadLocal<String>();
    //所有线程都可以获取里面的值
    private static String threadTest=new String();
    @RequestMapping(value = "/test",method = RequestMethod.POST)
    @ApiOperation("线程测试接口")
    public Result thread(){
        new Thread(new Runnable(){
            @Override
            public void run() {
//                threadLocal.set("你好啊");
                threadTest="你好啊";
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                te("A");
            }
        }).start();
        new Thread(new Runnable(){
            @Override
            public void run() {
                te("B");
            }
        }).start();
        return null;
    }
    public void te(String name){
        System.out.println(name+"线程说:"+threadTest);
    }
}
